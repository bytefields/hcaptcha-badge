# hCaptcha Badge

##Installation
Copy the js file into site folder, and add it's relative path using a script tag just before the `</body>` tag,

`<script type="text/javascript" src="/hcaptcha-badge.js?key=10000000-ffff-ffff-ffff-000000000001"></script>`

Or use the CDN (Statically.io) hosted script

`<script type="text/javascript" src="https://cdn.statically.io/gl/bytefields/hcaptcha-badge/main/hcaptcha-badge.js?key=10000000-ffff-ffff-ffff-000000000001"></script>`

## Usage
Refer tests folder for usage.

## Screenshot
![Screenshot](screenshot.png)

## To Do
* Translation
* Tests for manual mode
